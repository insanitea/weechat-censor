# Weechat script for censoring users.
# by wtfppl <origin206@pm.me>

try:
    import weechat
    WEECHAT_RC_OK = weechat.WEECHAT_RC_OK
    WEECHAT_RC_ERROR = weechat.WEECHAT_RC_ERROR
except ImportError:
    from sys import exit
    exit("This script is meant to be run under Weechat.")

import re
formatting_expr = re.compile("([\x02\x03\x1D\x1F\x16\x0F])(\d{1,2}(,\d{1,2}){0,1}){0,1}")

def reload_rules(*args):
    global rules
    rules = weechat.config_get_plugin("rules")
    if rules == None:
        rules = []
    else:
        rules = list(filter(lambda x: len(x) > 0, rules.split(",")))

    return WEECHAT_RC_OK

def save_rules():
    global rules
    weechat.config_set_plugin("rules", ",".join(rules))

# ----

def cmd_censor(data, buffer, rule):
    if len(rule) == 0 or "," in rule or ' ' in rule:
        weechat.prnt("", "[censor] malformed host rule")
        return WEECHAT_RC_ERROR

    global rules
    if rule not in rules:
        rules.append(rule)
        save_rules()
        weechat.prnt("", "[censor] rule added: '{0}'".format(rule))
    else:
        weechat.prnt("", "[censor] rule already exists")
        return WEECHAT_RC_ERROR

    return WEECHAT_RC_OK

def cmd_uncensor(data, buffer, rule):
    if len(rule) == 0 or "," in rule:
        weechat.prnt("", "[censor] malformed host rule")
        return WEECHAT_RC_ERR

    global rules
    for i,r in enumerate(rules):
        if r == rule:
            rules.pop(i)
            save_rules()
            weechat.prnt("", "[censor] rule removed: '{0}'".format(rule))
            return WEECHAT_RC_OK

    weechat.prnt("", "[censor] rule does not exist")
    return WEECHAT_RC_ERR

def uncensor_completion_rules(data, item, buffer, completion):
    global rules
    for rule in rules:
        weechat.hook_completion_list_add(completion, rule, 0, weechat.WEECHAT_LIST_POS_END)

    return WEECHAT_RC_OK

def censor_cb(data, mod, mod_data, line):
    host = weechat.info_get_hashtable("irc_message_parse", { "message": line })["host"]

    global rules
    for rule in rules:
        if re.search(rule, host):
            colour = weechat.config_get_plugin("colour")
            if colour == "":
                colour = "red"

            global formatting_expr
            i = line.find(" :") + 2
            return line[:i] + weechat.color(colour + "," + colour) + formatting_expr.sub("", line[i:]) + weechat.color("resetcolor")

    return line

if __name__ == "__main__":
    weechat.register("censor", "wtfppl", "0.1", "GPL3", "Script for censoring users.", "", "")

    weechat.hook_completion("uncensor_completion_rules", "rule completion for /uncensor", "uncensor_completion_rules", "")

    weechat.hook_command("censor", "Censor a user", "<host>", "regular expression for host", "%(irc_channel_nicks_hosts)", "cmd_censor", "")
    weechat.hook_command("uncensor", "Uncensor a user", "<host>", "regular expression for host", "%(uncensor_completion_rules)", "cmd_uncensor", "")

    weechat.hook_config("plugins.var.python.censor.*", "reload_rules", "")
    weechat.hook_modifier("irc_in2_privmsg", "censor_cb", "");

    reload_rules()
